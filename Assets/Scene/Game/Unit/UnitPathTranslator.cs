﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.TileMap;
using QPath;
using System;
using UnityEngine;
namespace Assets.Scene.Game.Unit
{
    public class UnitPathTranslator
    {
        private IQPathTile[] _currentPath;
        private int _nextIndex = 0;
        private IQPathTile _currentTile;
        private IQPathTile _nextTile;
        private UnitData _unit;
        private GameObject _objectToMove;
        private Action OnDestinationReached;

        public void SetPathDetails(IQPathTile[] path, GameObject obj, UnitData unit, Action onDestinationReached)
        {
            _currentPath = path;
            _currentTile = unit.CurrentTile;
            _nextIndex = 0;
            _unit = unit;
            _objectToMove = obj;
            OnDestinationReached = onDestinationReached;

        }

        public void MoveAlongPath()
        {
            SetupNextTile();

            if (_currentTile == GetDestinationTile())
            {
                SetDestinationReached();
            }
            else
            {
                MoveTowards();
            }
        }

        private void SetupNextTile()
        {
            if (_nextTile == null)
            {
                if (!(_nextIndex + 1 <= _currentPath.Length - 1))
                {
                    return;
                }
                _nextTile = _currentPath[_nextIndex + 1];
                _unit.CurrentTile = _nextTile;
            }
        }

        private IQPathTile GetDestinationTile()
        {
            return _currentPath[_currentPath.Length - 1];
        }

        private void SetDestinationReached()
        {
            _currentPath = null;
            _nextIndex = 0;

            _unit.CurrentTile = _currentTile;
            ((TileData)_currentTile).AddUnit(_unit);
            _unit.DestinationReached();

            var tile = ((TileData)_currentTile);

            if (OnDestinationReached != null)
            {
                OnDestinationReached();
            }
        }

        private void MoveTowards()
        {
            Vector3 destination = GetNextDestination();
            var tile = (TileData)_currentTile;
            var nextTile = (TileData)_nextTile;
       
            
            float maxDistanceToTravelThisTick = DistanceToTravelThisTick(_unit.Speed, Time.deltaTime);
         
            if (MapHelper.IsHill(tile) || MapHelper.IsHill(nextTile)) { maxDistanceToTravelThisTick = AlterSpeedByPercentage(maxDistanceToTravelThisTick, .20f); }
            if (MapHelper.IsMountain(tile)|| MapHelper.IsHill(nextTile)) { maxDistanceToTravelThisTick = AlterSpeedByPercentage(maxDistanceToTravelThisTick, .50f); }

            _objectToMove.transform.position = Vector3.MoveTowards(_objectToMove.transform.position, destination, maxDistanceToTravelThisTick);

            if (IsNextTileReached(_objectToMove.transform.position, destination))
            {
                SetNextTileReached();
            }
        }

        private float AlterSpeedByPercentage(float speed, float percentage)
        {
            speed -= speed * percentage;
            return speed;

        }

        private Vector3 GetNextDestination()
        {
            if (_nextTile == null)
            {
                Debug.Log("Next tile is null when getting next destingation");
                return Vector3.zero;
            };

            var convertedNextTile = (TileData)_nextTile;
            
            float xWorldCoords = GetXWorldCoords(convertedNextTile);
            float yWorldCoords = GetYWorldCoords(convertedNextTile);

            return new Vector3(xWorldCoords, 0, yWorldCoords);
        }

        private float GetYWorldCoords(TileData source)
        {
            return HexMapUtility.GetWorldYCoords(source.Y);
        }
        private float GetXWorldCoords(TileData source)
        {

            return HexMapUtility.GetWorldXCoords(source.X, source.Y);
        }

        private float DistanceToTravelThisTick(float speed, float timePassed)
        {
            return speed * timePassed;
        }
        private bool IsNextTileReached(Vector3 position, Vector3 target)
        {
            return (Vector3.Distance(position, target) < 0.01f);
        }

        private void SetNextTileReached()
        {
            var theCurrenttile = ((TileData)_currentTile);

            theCurrenttile.RemoveUnit(_unit);

            _currentTile = _nextTile;
            var newCurrentTile = ((TileData)_currentTile);

            _unit.CurrentTile = _nextTile;
            ((TileData)_currentTile).AddUnit(_unit);

            if (_nextIndex < _currentPath.Length - 1)
            {
                _nextIndex++;
                _nextTile = _currentPath[_nextIndex];
                
            }
        }
    }

}