﻿using Assets.Scene.Game.TileMap;
using UnityEngine;

namespace Assets.Scene.Game.Unit
{
    public class UnitBuilder : MonoBehaviour {

        public GameObject Unit_Prefab;

        public UnitBuilder(GameObject unit_Prefab)
        {
            Unit_Prefab = unit_Prefab;
        }

        public GameObject CreateUnitAt(int x, int y, Transform parent)
        {
            var pos = HexMapData.GetDataTile(x, y);

            var go_unit = Instantiate(Unit_Prefab, new Vector3(HexMapUtility.GetWorldXCoords(pos.X, pos.Y), 0, HexMapUtility.GetWorldYCoords(pos.Y)), Quaternion.identity);

            go_unit.transform.SetParent(parent);

            return go_unit;
        }
    }
}