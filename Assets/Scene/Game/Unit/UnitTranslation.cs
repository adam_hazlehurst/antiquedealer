﻿using Assets.Scene.Game.TileMap;
using UnityEngine;

namespace Assets.Scene.Game.Unit
{
    public class UnitTranslation
    {
        public void Move(HexTileDataWrapper destinationTileWrapper, GameObject selectedUnit)
        {
            if (destinationTileWrapper == null)
            {
                return;
            }
            var destination = HexMapData.GetDataTile(destinationTileWrapper.x, destinationTileWrapper.y);

            GetUnitRenderer(selectedUnit).Unit.Move_To(destination);
        }

        private UnitRenderer GetUnitRenderer(GameObject sourceGameObject)
        {
            return sourceGameObject.GetComponent<UnitRenderer>();
        }

    }

}