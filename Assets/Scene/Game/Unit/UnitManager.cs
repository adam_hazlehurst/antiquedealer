﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.TileMap;
using QPath;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scene.Game.Unit
{

    public class UnitManager
    {
        private Dictionary<UnitData, GameObject> _unitsToGameObjectHashMap;
        private Dictionary<GameObject, UnitData> _gameObjectToUnitDataHashMap;
        private UnitBuilder _unitBuilder;
        private static int _indexCount = 0;
        private IQPathWorld _world;

        public GameObject GetUnitGameObject(UnitData unit)
        {
            if (!_unitsToGameObjectHashMap.ContainsKey(unit))
            {
                return null;
            }
            return _unitsToGameObjectHashMap[unit];
        }

        public UnitData GetUnitGameObject(GameObject gameObject)
        {
            if (!_gameObjectToUnitDataHashMap.ContainsKey(gameObject))
            {
                Debug.LogError("Unit Manager:: does not contain unit");
                return null;
            }
            return _gameObjectToUnitDataHashMap[gameObject];
        }

        public int GetCountByOwner(int ownerId)
        {
            var counting = _unitsToGameObjectHashMap.Keys.ToList();
            return counting.Count(x => x.GetPlayerOwnerId() == ownerId);
        }

        public UnitManager(UnitBuilder unitBuilder)
        {
            _unitsToGameObjectHashMap = new Dictionary<UnitData, GameObject>();
            _gameObjectToUnitDataHashMap = new Dictionary<GameObject, UnitData>();
            _unitBuilder = unitBuilder;
            _world = CoreManager.instance.HexMapData;

        }

        public GameObject CreateUnit(IQPathTile spawnTile, Transform parent, int playerOwner)
        {

            _indexCount++;

            var spawnTileData = (TileData)spawnTile;

            var go_unit = _unitBuilder.CreateUnitAt(spawnTileData.X, spawnTileData.Y, parent);

            SetUpUnitData(spawnTile, go_unit, playerOwner);

            return go_unit;
        }

     
        private void SetUpUnitData(IQPathTile spawnTileLocation, GameObject unitCreated_go, int playerOwner)
        {
            var createdUnitData = new UnitData(_indexCount, (HexMapData)_world, (TileData)spawnTileLocation, GetUnitRenderer(unitCreated_go).OnUnitPathChanged);

            ((TileData)createdUnitData.CurrentTile).AddUnit(createdUnitData);

            createdUnitData.RegisterOnDestinationReached(OnDestinationReached);

            createdUnitData.SetPlayerOwner(playerOwner);

            _unitsToGameObjectHashMap[createdUnitData] = unitCreated_go;

            _gameObjectToUnitDataHashMap[unitCreated_go] = createdUnitData;

            GetUnitRenderer(unitCreated_go).Unit = createdUnitData;
            
        }

       

        public void OnDestinationReached(UnitData unit)
        {
            if (unit.GetPlayerOwnerId() == 0)
            {
                ((TileData)unit.CurrentTile).RemoveUnit(unit);
                Destroy(unit);
            }
        }

        public void Destroy(IQPathUnit unitToDestroy)
        {
            var unit = (UnitData)unitToDestroy;
                        
            if (!_unitsToGameObjectHashMap.ContainsKey(unit))
            {
                Debug.LogError("UnitManager::Destroy(Unit) cant find unit in  Dictionary");
                return;
            }

            var unit_go = _unitsToGameObjectHashMap[unit];

            unit_go.GetComponentInChildren<UnitRenderer>().Destroy();

            _unitsToGameObjectHashMap.Remove(unit);
            _gameObjectToUnitDataHashMap.Remove(unit_go);
        }

        private UnitRenderer GetUnitRenderer(GameObject gameObject)
        {
            return gameObject.GetComponentInChildren<UnitRenderer>();
        }

        
    }
}
