﻿using UnityEngine;

namespace Assets.Scene.Game.Unit
{
    public class UnitFlashEmitter
    {

        public void Begin(GameObject gameObjectToBlink)
        {
            var selectedFlashControl = GetFlashRenderer(gameObjectToBlink);

            if (!FlashRenderExists(selectedFlashControl))
                return;

            selectedFlashControl.Begin();
        }

        public void End(GameObject gameObjectToStopBlinking)
        {
            var selectedFlashControl = GetFlashRenderer(gameObjectToStopBlinking);

            selectedFlashControl.End();
        }

        private FlashRenderer GetFlashRenderer(GameObject gameObjectToBlink)
        {
            return gameObjectToBlink.GetComponentInChildren<FlashRenderer>();
        }

        public bool FlashRenderExists(FlashRenderer selectedFlashControl)
        {
            if (selectedFlashControl == null)
            {
                Debug.LogError("MouseManager:: MakeItFlash(). could not find a renderer");
                return false;
            }
            return true;
        }

    }
}



