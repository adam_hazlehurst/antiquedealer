﻿using UnityEngine;

namespace Assets.Scene.Game.Unit
{
    public class FlashRenderer : MonoBehaviour
    {

        public bool isFlashing = false;
        private Material materialToFlash;
        public float FlashDelayPeriod;
        private float cooldown;
        private bool _coolDownSpanComplete = false;
        public Color flashOnColor = Color.white;
        public Color naturalColor;


        // Use this for initialization
        void Start()
        {
            cooldown = FlashDelayPeriod;
            materialToFlash = GetComponent<Renderer>().material;
            naturalColor = gameObject.GetComponentInChildren<Renderer>().material.color;
        }

        // Update is called once per frame
        void Update()
        {
            if (isFlashing)
            {
                SubtractCooldown(Time.deltaTime);
                
                if (cooldown > 0f)
                {
                    ChangeColorOfFlash();
                }
                else
                {
                    ToggleFlash();
                    cooldown = FlashDelayPeriod;
                }
            }
        }

        public void Begin()
        {
            isFlashing = true;
        }

        public void End()
        {
            isFlashing = false;
            SetMaterialColor(naturalColor);

        }

        private void SubtractCooldown(float timePassed)
        {
            cooldown -= timePassed;
        }

        private void ChangeColorOfFlash()
        {
            
            if (_coolDownSpanComplete)
            {
                SetMaterialColor(flashOnColor);
            }
            else
            {
                SetMaterialColor(naturalColor);
            }
        }

        private void SetMaterialColor(Color color)
        {
            materialToFlash.color = color;
        }

        private void ToggleFlash()
        {
            _coolDownSpanComplete = !_coolDownSpanComplete;
         
        }
       
    }
}
