﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.TileMap;
using Assets.Scene.Game.Unit;
using QPath;
using UnityEngine;

public class UnitRenderer : MonoBehaviour {

    public UnitData Unit;
    private UnitPathTranslator _unitPathTranslator;

    private IQPathTile[] _currentPath;
    private bool _shouldUnitMove = false;

    void Start()
    {
        ResetColor();
    }

    void Update()
    {
        if (_shouldUnitMove)
        {
            _unitPathTranslator.MoveAlongPath();
        }
    }

    private void OnDestinationReached()
    {
        _shouldUnitMove = false;
    }

    public void OnUnitPathChanged(IQPathTile[] path)
    {
        if (_currentPath != path)
        {
            _currentPath = path;
            _shouldUnitMove = true;
            _unitPathTranslator = new UnitPathTranslator();
            _unitPathTranslator.SetPathDetails(path,this.gameObject, Unit, OnDestinationReached);
        }
    }

    private Color ConvertToColor(ColorEnum color)
    {
        Color unit_color = Color.white;
        if (color == ColorEnum.BLUE)
        {
            unit_color = Color.blue;
        }
        if (color == ColorEnum.RED)
        {
            unit_color = Color.red;
        }
        if (color == ColorEnum.ORANGE)
        {
            unit_color = new Color(1, .6f, .2f);
        }
        if (color == ColorEnum.BROWN)
        {
            unit_color = new Color(.6f, .4f, .2f);
        }
        if (color == ColorEnum.PINK)
        {
            unit_color = new Color(1, .2f, .9f);
        }
        if (color == ColorEnum.YELLOW)
        {
            unit_color = Color.yellow;
        }
        if(color == ColorEnum.TURQUIOSE)
        {
            float r, g, b;
            r = 64f / 255f;
            g = 224f / 255f;
            b = 208f / 255f;

            unit_color = new Color(r,g,b);
        }
        if(color == ColorEnum.GREEN)
        {
            unit_color = Color.green;
        }
        
        return unit_color;

    }

    public Color GetColor()
    {
        return  ConvertToColor(GetColorToUse());
        
    }

    private ColorEnum GetColorToUse()
    {
        var ownerId = Unit.GetPlayerOwnerId();

        if (ownerId == 0)
        {
           return  ColorEnum.WHITE;
        }
        
        return  CoreManager.instance.PlayerManager.Get(Unit.GetPlayerOwnerId()).Color;

    }

    public void ResetColor()
    {
        var renderer = GetComponentInChildren<Renderer>();

        var unit_color = ConvertToColor(GetColorToUse());

        renderer.material.color = unit_color;
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}

