﻿namespace Assets.Scene.Game.TileMap
{
    public static class UnityMathHelper
    {

        public static float RandomFloat01()
        {
            return UnityEngine.Random.Range(0f, 1f);
        }
        public static int Range(int min, int max)
        {
            return UnityEngine.Random.Range(min, max);
        }
        public static float Range(float min, float max)
        {
            return UnityEngine.Random.Range(min, max);
        }
    }
}
