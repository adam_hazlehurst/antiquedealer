﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.TileMap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleInCityRenderer : MonoBehaviour {

    public GameObject UiPlacard;
    private bool _isVisible = false;
    private TileData _tileData;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (_tileData.GetUnits().Count > 0)
        {
            _isVisible = true;
        }
        else
        {
            _isVisible = false;
        }
        //can i call the city to see if there is a unit present

        if (_isVisible)
        {
            UiPlacard.SetActive(true);
        }
        else
        {
            UiPlacard.SetActive(false);
        }

	}

    public void SetTile(TileData tileData)
    {
        _tileData = tileData;
    }

    public void OnVehicleEntered(List<UnitData> units, UnitData arrivedUnit)
    {
        if (units.Count > 0)
        {
            _isVisible = true;
        }
        
    }

    public void OnVehicleDeparted(List<UnitData> units, UnitData arrivedUnit)
    {
        if (units.Count <= 0)
        {
            _isVisible = false;
        }
    }
    
}
