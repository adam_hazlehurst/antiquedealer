﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.TileMap;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scene.Game.Cities
{
    public class CityManager
    {
        private static int count = 0;

        private Dictionary<City, GameObject> _cities;
        private CityBuilder _cityBuilder;
        private CityObjectLoader _cityObjectLoader;
        public int TotalCityCount;

        public CityManager()
        {
            _cities = new Dictionary<City, GameObject>();
            _cityBuilder = new CityBuilder();
            _cityObjectLoader = new CityObjectLoader();
        }

        
        public void CreateCity(Transform parent, TileData dataTile)
        {
            count++;

            dataTile.SetNeighboursAsCityResearved();

            City city = _cityBuilder.Build(count, dataTile.X, dataTile.Y);

            GameObject city_go = _cityObjectLoader.Create("city", parent, new Vector3(HexMapUtility.GetWorldXCoords(dataTile.X, dataTile.Y), 0, HexMapUtility.GetWorldYCoords(dataTile.Y)));

            var vehicleInCityRenderer  = city_go.GetComponentInChildren<VehicleInCityRenderer>();

            vehicleInCityRenderer.SetTile(dataTile);

            city.SetName("Some City"+city.GetId());

            city_go.GetComponentInChildren<CityNameRenderer>().CityName.text = city.GetName();
            
            dataTile.SetCity(city);
            
            _cities.Add(city, city_go);
            
        }
        
        public  GameObject GetCity(City city)
        {
            return _cities[city];
        }

        public void GenerateStartCities(int maxX, int maxY,int count, Transform parent)
        {
            int cantPlaceCityCount = 0;
            TotalCityCount = count;
            for (int x = 0; x < count; x++)
            {
                Vector3 randomCityPosition  = new Vector3(UnityEngine.Random.Range(1, maxX - 1),0, UnityEngine.Random.Range(1, maxY - 1));

                var dataTile = HexMapData.GetDataTile((int)randomCityPosition.x, (int)randomCityPosition.z);
                if (dataTile.elevation < 0f || dataTile.elevation > .3f)
                {
                    x--;
                    cantPlaceCityCount++;
                    if (cantPlaceCityCount > 100)
                    {
                        Debug.LogError("Cant place cities!!! due to water || hill || mountain");
                        break;
                    }
                    continue;
                }

                if (dataTile.IsResearved())
                {
                    x--;
                    continue;
                }

                CreateCity(parent, dataTile);
            }
        }

        public City GetRandomCity()
        {
             var rnd = UnityEngine.Random.Range(1, _cities.Keys.Count());
            
            return _cities.Keys.ToList()[rnd - 1];
        }

        public Dictionary<City, GameObject> GetCities()
        {
            return _cities;
        }
    }
}
