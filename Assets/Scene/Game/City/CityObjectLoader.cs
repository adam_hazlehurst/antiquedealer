﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scene.Game.Cities
{
    public class CityObjectLoader
    {
        private Dictionary<string, GameObject> _gameObjectLibrary;

        public CityObjectLoader()
        {
            _gameObjectLibrary = new Dictionary<string, GameObject>();
            LoadFromFile();
        }

        private void LoadFromFile()
        {
            var city  = Resources.Load<GameObject>("City/City");
            _gameObjectLibrary.Add("city", city);
        }

        public GameObject Create(string name, Transform parent, Vector3 position)
        {
            if(!_gameObjectLibrary.ContainsKey(name))
            {
                Debug.LogError("Could not find " + name + " in CityObjectLoader");
            }

            Quaternion rotation = Quaternion.Euler(0f, UnityEngine.Random.Range(0f, 360f), 0f);

            GameObject city_go = MonoBehaviour.Instantiate(_gameObjectLibrary[name], position, Quaternion.identity);

            city_go.transform.Find("Graphics").rotation = rotation;

            city_go.transform.SetParent(parent);

            return city_go;
        }
    }
}
