﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityMouseOver : MonoBehaviour {


    private Color _colorToHighliteObject = Color.yellow;
    private Color _naturalColorOfObject;

    
    public Renderer rendererToChange;

    private bool IsHighliting = false;
	// Use this for initialization
	void Start () {
        _naturalColorOfObject = rendererToChange.material.color;
	}
	
	// Update is called once per frame
	void Update () {
        if (IsHighliting)
        {
            rendererToChange.material.color = _colorToHighliteObject;
        }
        else
        {
            rendererToChange.material.color = _naturalColorOfObject;
        }
	}

    public void TurnOn()
    {
        IsHighliting = true;
    }
    public void TurnOff()
    {
        IsHighliting = false;
    }
}
