﻿using Assets.Scene.Game.Models;

namespace Assets.Scene.Game.Cities
{
    public class CityBuilder
    {

        public City Build(int id, int x, int y)
        {
            return new City(id, x,y);
        }
    }
}
