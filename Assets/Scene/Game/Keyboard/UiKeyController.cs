﻿using Assets.Scene.Game.TileMap;
using UnityEngine;

public class UiKeyController : MonoBehaviour {

    public GameObject DebugPanel;
    public TileData SelectedTile;
	
	// Update is called once per frame
	void Update () {


        if (Input.GetKeyUp(KeyCode.F12))
        {
            DebugPanel.SetActive(!DebugPanel.activeSelf);
        }
	}
}
