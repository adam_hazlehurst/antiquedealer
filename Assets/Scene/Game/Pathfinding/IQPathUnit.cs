﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QPath
{
    public interface IQPathUnit
    {
        float CostToEnterTile(IQPathTile sourceTile, IQPathTile destinationTile);
    }
}
