﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace QPath
{
    public class QPathAStar
    {
        private readonly IQPathWorld _world;
        private readonly IQPathUnit _pathUnit;
        private readonly IQPathTile _startTile;
        private readonly IQPathTile _endTile;
        Queue<IQPathTile> _path;
        CostEstimateDelegate _costEstimateFunc;

        public QPathAStar(IQPathWorld world, IQPathUnit unit, IQPathTile startTile, IQPathTile endTile, CostEstimateDelegate costEstimateFunc)
        {
            //Do Setup
            _world = world;
            _pathUnit = unit;
            _startTile = startTile;
            _endTile = endTile;
            _costEstimateFunc = costEstimateFunc;

            //Should we create a graph.
            
        }

        public void DoWork()
        {
            _path = new Queue<IQPathTile>();

            HashSet<IQPathTile> closedSet = new HashSet<IQPathTile>();

            PathfindingPriorityQueue<IQPathTile> openSet = new PathfindingPriorityQueue<IQPathTile>();
            openSet.Enqueue(_startTile, 0);

            Dictionary<IQPathTile, IQPathTile> came_from = new Dictionary<IQPathTile, IQPathTile>();

            //cost to get to the start tile.
            Dictionary<IQPathTile, float> g_score = new Dictionary<IQPathTile, float>();
            g_score[_startTile] = 0;

            //estimated cost to get to the tile. (power of a-star)
            Dictionary<IQPathTile, float> f_score = new Dictionary<IQPathTile, float>();
            f_score[_startTile] = _costEstimateFunc(_startTile, _endTile);

            while (openSet.Count > 0)
            {
                IQPathTile current = openSet.Dequeue();

                if(current == _endTile)
                {
                    ReconstructPath(came_from, current);
                    return;
                }

                closedSet.Add(current);

                foreach(IQPathTile edge_neighbour in current.GetNeighbours())
                {
                    IQPathTile neighbour = edge_neighbour;
                    if (closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    float total_pathfinding_cost_to_neighbour = neighbour.AggregateCostToEnter(g_score[current], current, _pathUnit);

                    float tentative_g_score = total_pathfinding_cost_to_neighbour;

                    if(openSet.Contains(neighbour) && tentative_g_score >= g_score[neighbour])
                    {
                        continue;
                    }

                    //this is either a new tile or its a cheaper(better) route.
                    came_from[neighbour] = current;
                    g_score[neighbour] = tentative_g_score;
                    f_score[neighbour] = g_score[neighbour] + _costEstimateFunc(neighbour, _endTile);

                    openSet.EnqueueOrUpdate(neighbour, f_score[neighbour]);
                }


            }

        }

        private void ReconstructPath(Dictionary<IQPathTile, IQPathTile> came_from, IQPathTile current)
        {
            Queue<IQPathTile> total_path = new Queue<IQPathTile>();
            total_path.Enqueue(current);

            while (came_from.ContainsKey(current))
            {
                current = came_from[current];
                total_path.Enqueue(current);
            }

            _path = new Queue<IQPathTile>(total_path.Reverse());
        }

       

        public IQPathTile[] GetList()
        {
            return _path.ToArray();
        }
    }
}
