﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QPath
{

    /// <summary>
    ///     Tile[] ourPath = QPath.FindPath( theWorld, theUnit,  startTile, endTile );
    ///     
    ///     theUnit is a thing trying to path between tiles. it could have
    ///     special logic effecting how it moves between tiles.
    ///     
    ///     our tiles must return the following.
    ///     1. list of neighbours
    ///     2. cost to enter the tile from other tile.
    /// 
    /// </summary>
    public static class QPath
    {
        public static IQPathTile[] FindPath(IQPathWorld world, IQPathUnit unit, IQPathTile startTile, IQPathTile endTile, CostEstimateDelegate costEstimateFunc )
        {
            if (world == null || unit == null || startTile == null || endTile == null)
            {
                Debug.LogError("Null values passed to QPath::FindPath");
                return null;
            }

            QPathAStar resolver = new QPathAStar(world, unit, startTile, endTile, costEstimateFunc);

            resolver.DoWork();

            return resolver.GetList();
        }

    }

    public delegate float CostEstimateDelegate(IQPathTile a, IQPathTile b);

    
}
