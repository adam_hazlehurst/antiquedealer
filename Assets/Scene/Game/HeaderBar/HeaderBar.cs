﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.Player;
using System.Linq;
using TMPro;
using UnityEngine;


public class HeaderBar : MonoBehaviour {

    private Player _player;
    public TextMeshProUGUI textMeshCash;
    public TextMeshProUGUI textTime;

    public void AlterSpeed(float speed)
    {
        if(speed < .4f) { speed = 0.0f; }
        Time.timeScale = speed;
    }
    
	// Use this for initialization
	void Start () {
        _player = CoreManager.instance.PlayerManager.Get(0);
	}
	
	// Update is called once per frame
	void Update () {
		if(_player != null)
        {
            textMeshCash.text = _player.Cash.ToString("0.00");
        }
        textTime.text = CoreManager.instance.GameStatus.GetTimeAndDay();
	}
}
