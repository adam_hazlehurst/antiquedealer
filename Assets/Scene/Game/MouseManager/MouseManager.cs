﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.TileMap;
using Assets.Scene.Game.Unit;
using QPath;
using System;
using System.Linq;
using UnityEngine;

public class MouseManager : MonoBehaviour {

    private GameObject SelectedUnit;
    private GameObject _selectedCity;
    private Color _selectedCityNaturalColor;
    private UnitTranslation _unitTranslation;
    private UnitData _newSelectedUnit;
    private UnitData _lastSelectedUnit;

    LineRenderer LineRenderer;
    public LayerMask LayerIdForHexTiles;
    public LayerMask UiLayerMask;

    private TileData _tileUnderMouse;
    private TileData _tileLastUnderMouse;

    private UnitFlashEmitter _flashEmitter;
    private PathLineRenderer _unitPathRenderer;

    private void Start()
    {
        _flashEmitter = new UnitFlashEmitter();
        _unitTranslation = new UnitTranslation();
        LineRenderer = transform.GetComponentInChildren<LineRenderer>();
        _unitPathRenderer = new PathLineRenderer();
    }
    // Update is called once per frame
    void Update () {

        _tileUnderMouse = MouseToTile();
             
        HighlightCityUnderMouse();
        DeselectCityPreviouslyUnderMouse();


        RaycastHit hitInfo;
        
        if (Physics.Raycast(GetRayAtMouseCoordinate(), out hitInfo))
        {

            if (IsLeftMouseUp())

            {
                ToggleBlinkForSelectedUnit();
                if (_newSelectedUnit != null)
                {
                    _unitPathRenderer.Draw(LineRenderer, _newSelectedUnit.GetCurrentPath(), _newSelectedUnit);
                }
                var debug = GameObject.Find("DebugPanel");
                if (debug != null)
                {
                    debug.GetComponentInChildren<TileSelectedDubugUI>().SetTile(_tileUnderMouse);
                }
            }
           

            if (IsRightMouseUp())
            {
                if (_newSelectedUnit != null)
                {
                    var go = CoreManager.instance.UnitManager.GetUnitGameObject(_newSelectedUnit);
                    _unitTranslation.Move(GetHexTileDataWrapper(hitInfo), go);
                    var path = _newSelectedUnit.GetCurrentPath();

                    _unitPathRenderer.Draw(LineRenderer, path, _newSelectedUnit);
                }
            }
        }

        if (Physics.Raycast(GetRayAtMouseCoordinate(), out hitInfo, LayerMask.GetMask("UI")))
        {
        }
        
        _tileLastUnderMouse = _tileUnderMouse;

    }


    private void HighlightCityUnderMouse()
    {
        if (_tileUnderMouse != null && _tileUnderMouse.GetCity() != null)
        {
            _selectedCity = CoreManager.instance.CityManager.GetCity(_tileUnderMouse.GetCity());
            _selectedCity.GetComponentInChildren<CityMouseOver>().TurnOn();
        }
    }
    private void DeselectCityPreviouslyUnderMouse()
    {

        if (_tileUnderMouse != null && _tileUnderMouse.GetCity() == null && _selectedCity != null)
        {
            _selectedCity.GetComponentInChildren<CityMouseOver>().TurnOff();
        }
    }

    private bool IsLeftMouseUp()
    {
        return Input.GetMouseButtonUp(0);
    }
    private bool IsRightMouseUp()
    {
        return Input.GetMouseButtonUp(1);
    }

    private HexTileDataWrapper GetHexTileDataWrapper(RaycastHit hitInfo)
    {
        return hitInfo.collider.gameObject.GetComponentInChildren<HexTileDataWrapper>();
    }

    private Ray GetRayAtMouseCoordinate()
    {
        return Camera.main.ScreenPointToRay(Input.mousePosition); 
    }

    private TileData MouseToTile()
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        int layerMask = LayerIdForHexTiles.value;

        if(Physics.Raycast(mouseRay, out hitInfo, Mathf.Infinity, layerMask))
        {
            GameObject hex = hitInfo.rigidbody.gameObject;

            return CoreManager.instance.GetMap().GetTile(hex);

        }
        return null;
    }

    private UnitData GetTheSelectedUnit()
    {
        if (_tileLastUnderMouse == null)
            return null;
        var unitsOnSelectedTile = _tileUnderMouse.GetUnits();
        int minExpectedUnits = 0;

        if (unitsOnSelectedTile.Count() > minExpectedUnits)
        {
            _newSelectedUnit = unitsOnSelectedTile[0];
            return _newSelectedUnit;
        }
        return null;

    }

    private GameObject GetUnitDatasGameObject(UnitData unitData)
    {
        return CoreManager.instance.UnitManager.GetUnitGameObject(unitData);
    }

    private bool WasNewUnitSelected()
    {
        return (_lastSelectedUnit != _newSelectedUnit);
      
    }
    
    private void ToggleBlinkForSelectedUnit()
    {
        _newSelectedUnit = GetTheSelectedUnit();

        if (_newSelectedUnit == null) { return; }

        _flashEmitter.Begin(GetUnitDatasGameObject(_newSelectedUnit));

        if (WasNewUnitSelected())
        {
           
            if (_lastSelectedUnit != null)
            {
                _flashEmitter.End(GetUnitDatasGameObject(_lastSelectedUnit));
            }
        }

        _lastSelectedUnit = _newSelectedUnit;
    }
}
