﻿using Assets.Scene.Game.TileMap;
using QPath;
using System;
using UnityEngine;

namespace Assets.Scene.Game.Models
{
    public class UnitData : IQPathUnit
    {
        private HexMapData _world;
        public IQPathTile CurrentTile;
        private IQPathTile[] _currentPath;
        Action<IQPathTile[]> OnPathChanged;
        private int _playerOwner;
        private int _id;
        public float Speed;

        private Action<UnitData> OnDestinationReached;


        public UnitData(int id, HexMapData world, TileData currentTile, Action<IQPathTile[]> OnPathChangedFunc)
        {
            _id = id;
            _world = world;
            CurrentTile = currentTile;
            OnPathChanged = OnPathChangedFunc;
            Speed = 2f;
        }

        public void RegisterOnDestinationReached(Action<UnitData> destinationReached)
        {
            OnDestinationReached += destinationReached;
        }

        public void UnRegiesterOnDestinationReached(Action<UnitData> cbdestinationReached)
        {
            OnDestinationReached -= cbdestinationReached;
        }

        public TileData GetCurrentTile()
        {
            return (TileData)CurrentTile;
        }

        public void SetPlayerOwner(int playerOwner)
        {
            _playerOwner = playerOwner;
        }
        public int GetPlayerOwnerId()
        {
            return _playerOwner;
        }

        public int GetId()
        {
            return _id;
        }

        public float CostToEnterTile(IQPathTile sourceTile, IQPathTile destinationTile)
        {
            return 1f;
        }

        public IQPathTile[] GetCurrentPath()
        {
            return _currentPath;
        }

        public void Move_To(IQPathTile destination)
        {
            
            if (destination != null)
            {
                _currentPath =  QPath.QPath.FindPath(_world, this, CurrentTile, destination, TileData.Distance);
                if (OnPathChanged != null)
                {
                    OnPathChanged(_currentPath);
                }
            }
        }

      
        public void DestinationReached()
        {
            if (OnDestinationReached != null)
            {
                OnDestinationReached(this);
            }
        }
    }
}
