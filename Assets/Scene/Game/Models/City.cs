﻿using Assets.Scene.Game.TileMap;
using System;

namespace Assets.Scene.Game.Models
{
    public class City
    {
        private int _id;
        private string _name;
        public int X;
        public int Y;

        public City(int id, int x, int y)
        {
            X = x;
            Y = y;
            _id = id;
        }


        public string GetName()
        {
            return _name;
        }

        public int GetId()
        {
            return _id;
        }

        public void SetName(string name)
        {
            _name = name;
        }

    }
}
