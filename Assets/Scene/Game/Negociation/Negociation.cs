﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scene.Game.Negociation
{


    public class Negociation
    {

        public BasicList<GameThing> TeamBuyers;
        public BasicList<GameThing> TeamSellers;
        public BasicList<GameThing> Basket;

        public Negociation(BasicList<GameThing> buyers, BasicList<GameThing> sellers, BasicList<GameThing> basket)
        {
            TeamBuyers = buyers;
            TeamSellers = sellers;
            Basket = basket;

            AutoFillForTest();
        }


        public void AutoFillForTest()
        {
            var p = new Person();
            p.SetName("Buyer Fred");
            TeamBuyers.Add(p);

            var t = new Person();
            t.SetName("Seller Jane");
            TeamSellers.Add(t);

            var i = new Item();
            i.SetName("Flute");
            Basket.Add(i);
        }



    }

    public interface IGameThing
    {
        string GetName();
        void SetName(string arg);
    }

    public abstract class GameThing : IGameThing
    {
        private string _name;

        public string GetName()
        {
            return _name;
        }
        public void SetName(string arg)
        {
            _name = arg;
        }
    }

    public class Person : GameThing
    {
       
    }

    public class Item : GameThing
    {
     
    }

    public class BasicList<T> where T : IGameThing
    {
        protected List<T> _things;
        private Action<T> OnAdded;
        private Action<T> OnRemoved;

        public BasicList()
        {
            _things = new List<T>();
        }

        public void Register_OnAdded(Action<T> cbOnAdded)
        {
            OnAdded += cbOnAdded;
        }
        public void Register_OnRemoved(Action<T> cbOnRemoved)
        {
            OnRemoved += cbOnRemoved;
        }

        public void Add(T item)
        {
            if (!_things.Contains(item))
            {
                _things.Add(item);
                if (OnAdded != null)
                {
                    OnAdded(item);
                }
            }
        }

        public void AddRange(IEnumerable<T> items)
        {
            _things.AddRange(items);
        }

        public void Remove(T item)
        {
            if (_things.Contains(item))
            {
                _things.Remove(item);
                if (OnRemoved != null)
                {
                    OnRemoved(item);
                }
            }
        }

        public int Count()
        {
            return _things.Count();
        }

        public T GetItemAt(int index)
        {
            return _things[index];
        }
        
    }

    public class ItemAssessor
    {

        public void AssessItem(BasicList<Person> assessors, List<Item> Items)
        {

        }
    }

    
}
