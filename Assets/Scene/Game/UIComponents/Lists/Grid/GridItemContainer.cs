﻿using System.Collections.Generic;
using UnityEngine;

public class GridItemContainer : MonoBehaviour {


    public GameObject Container;
    public GameObject ContainerItem;
    private List<GameObject> _instanciatedItems;

    public List<string> Source;

    private bool _isUpdated = true;

    private void Awake()
    {
        _instanciatedItems = new List<GameObject>();
    }

    private void Update()
    {
        if (_isUpdated)
        {
            Clear();
            Draw();

            _isUpdated = false;
        }
    }

    private void Draw()
    {
        if(Source != null && Source.Count > 0)
        {
            for(int x = 0; x<= Source.Count; x++)
            {
                var go = Instantiate(ContainerItem);
                go.transform.SetParent(Container.transform);
                _instanciatedItems.Add(go);
            }
        }
    }

    public void SetSource(List<string> source)
    {
        if (Source != source)
        {
            Source = source;
            _isUpdated = true;

        }
    }

    private void Clear()
    {
        if (_instanciatedItems == null)
        {
            return;
        }
        for (int i = 0; i < _instanciatedItems.Count; i++)
        {
            Destroy(_instanciatedItems[i]);
        }
    }

    public void AddToSource(string arg)
    {
        Source.Add(arg);
        Clear();
        Draw();
    }
}
