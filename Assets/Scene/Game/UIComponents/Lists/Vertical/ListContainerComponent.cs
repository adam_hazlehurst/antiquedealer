﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListContainerComponent : MonoBehaviour
{
    public GameObject Container;
    public GameObject ContainerItem;
    private List<GameObject> _instanciatedItems;

    public List<string> _source;
    
    private bool _isUpdated = true;
    private void Awake()
    {
        _instanciatedItems = new List<GameObject>();
    }


    private void Update()
    {
        if (!IsSourceValid())
            return;
        if (_isUpdated)
        {
            Clear();
            Draw();
            _isUpdated = false;
        }
    }

    public void SetSource(List<string> source)
    {
        _source = source;

        if (!IsSourceValid())
            return;

        Refresh();
    }

    private void AddRenderItem(string name)
    {
        var go = Instantiate(ContainerItem);
        go.transform.SetParent(Container.transform);
        var textChildren = go.GetComponentsInChildren<Text>();

        textChildren[0].text = name;

        _instanciatedItems.Add(go);

    }
    public void AddItemToList(string arg)
    {
        _source.Add(arg);
        Refresh();
    }

    public void Refresh()
    {
        Clear();
        Draw();
    }

    public void Remove()
    {

    }

    public void Clear()
    {
        for (int x = 0; x < _instanciatedItems.Count; x++)
        {
            Destroy(_instanciatedItems[x]);
        }
    }
    private bool IsSourceValid()
    {
        if (_source == null)
            return false;
        if (_source.Count == 0)
            return false;

        return true;
    }

    public void Draw()
    {
        for (int x = 0; x < _source.Count; x++)
        {
            AddRenderItem(_source[x]);
        }
    }

}




