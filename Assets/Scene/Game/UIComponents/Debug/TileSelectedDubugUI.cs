﻿using Assets.Scene.Game.TileMap;
using TMPro;
using UnityEngine;

public class TileSelectedDubugUI : MonoBehaviour {

    public TextMeshProUGUI TextTileName;
    public TextMeshProUGUI TextXCoord;
    public TextMeshProUGUI TextYCoord;

    public TextMeshProUGUI textElevationValue;
    public TextMeshProUGUI textElevationName;

    public TextMeshProUGUI textTemperatureValue;
    public TextMeshProUGUI textTemperatureName;

    private TileData _tileData;
    private bool _changed = false;
   
	
	void Update () {
        if (_tileData != null)
        {
            if (_changed)
            {
                TextTileName.text = CoreManager.instance.GetMap().GetTile(_tileData).name;
                TextXCoord.text = "x: "+_tileData.X.ToString();
                TextYCoord.text = "y: "+_tileData.Y.ToString();
                textElevationValue.text = _tileData.elevation.ToString("0.00");
                textElevationName.text = _tileData.GetElevationDisplay();
                textTemperatureValue.text = _tileData.temperature.ToString("0.00");
                textTemperatureName.text = _tileData.GetTemperatureDisplay();
                _changed = false;
            }
        }
	}


    public void SetTile(TileData tileData)
    {
        if(_tileData!= tileData)
        {
            _tileData = tileData;
            _changed = true;
        }
    }
}
