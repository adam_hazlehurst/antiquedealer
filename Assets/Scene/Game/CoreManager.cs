﻿using Assets.Scene.Game.Cities;
using Assets.Scene.Game.Player;
using Assets.Scene.Game.TileMap;
using Assets.Scene.Game.Unit;
using System;
using UnityEngine;

public class CoreManager : MonoBehaviour {

    public static CoreManager instance;
    public PlayerManager PlayerManager;
    public HexMapData HexMapData;
    public CityManager CityManager;
    public UnitManager UnitManager;
    public GameStatus GameStatus;
    
    public TileMaterialLoader TileMaterialLoader;
    private PlayerLoader _playerLoader;

    public GameObject Unit_prfab;
    public GameObject hexPrefab;

    private Map _map;

    public int MapWidth = 25;
    public int MapHeight =30;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            GameStatus = new GameStatus();
            TileMaterialLoader = new TileMaterialLoader();
            HexMapData = new HexMapData(MapWidth, MapHeight, TileMaterialLoader);

            PlayerManager = new PlayerManager();
            _playerLoader = new PlayerLoader(new PlayerConfiguration(4), PlayerManager); 

            CityManager = new CityManager();
            var xMax = MapWidth - 1;
            var yMax = MapHeight - 1;
            CityManager.GenerateStartCities(xMax, yMax, 10, this.gameObject.transform);

            _map = new Map(this.gameObject, HexMapData, CityManager,  TileMaterialLoader);
            UnitManager = new UnitManager(new UnitBuilder(Unit_prfab));
            _map.RenderMap();
            _playerLoader.Load();

        }

    }
    void Update()
    {
        GameStatus.Update(Time.deltaTime);
    }


    public Map GetMap()
    {
        return _map;
    }
}

public class GameTimeKeeper
{
    private float secondsPerHour = 1f; //seconds
    private float tickTimePassed = 1f;
    private float hoursInDay = 24;
    private float hoursPassed = 0;

    private Action _onHourTick;
    private Action _onDayTick;

    private DateTime _currentDate;
    private DateTime _previousDate;

    public GameTimeKeeper()
    {
        _currentDate = new DateTime(1, 1, 1);
        _previousDate = _currentDate;
    }

    public string TimeAndDay()
    {
        return _currentDate.ToString("\\Yy \\MM \\Dd");
    }

    public void Register_OnHourTick(Action onHourTick)
    {
        _onHourTick += onHourTick;
    }
    public void Register_OnDailyTick(Action onDailyTick)
    {
        _onDayTick += onDailyTick;
    }

    public void Tick(float timePassed)
    {
        tickTimePassed -= timePassed;

        if (tickTimePassed <= 0)
        {
            HourlyTick();
            tickTimePassed = secondsPerHour;
        }
        if(_currentDate.Day == 1)
        {
            DailyTick();
        }
        if(_currentDate.Month != _previousDate.Month)
        {
            MonthTick();
        }
        if(_currentDate.Year != _previousDate.Year)
        {
            YearTick();
        }

    }

    private void HourlyTick()
    {
        if (_onHourTick != null)
        {
            _onHourTick();
        }
        _previousDate = _currentDate;
        _currentDate = _currentDate.AddHours(1);
        

    }

    private void DailyTick()
    {

        if (_onDayTick != null)
        {
            _onDayTick();
        }
    }
    private void MonthTick()
    {

        //if (_onDayTick != null)
        //{
        //    _onDayTick();
        //}
    }
    private void YearTick()
    {

        //if (_onDayTick != null)
        //{
        //    _onDayTick();
        //}
    }
}

public class GameStatus
{
    private GameTimeKeeper _timeKeeper;

    public GameStatus()
    {
        _timeKeeper = new GameTimeKeeper();
        
    }

    public void Register_OnDayEvent(Action onDayEvent)
    {
        _timeKeeper.Register_OnDailyTick(onDayEvent);
    }

    public void Register_OnHourEvent(Action onHourEvent)
    {
        _timeKeeper.Register_OnHourTick(onHourEvent);
    }

    public void Update(float timePassed)
    {
        _timeKeeper.Tick(timePassed);
    }

    public string GetTimeAndDay()
    {
       return  _timeKeeper.TimeAndDay();
    }

}









