﻿
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float panSpeed = 20f;
    public float panBorderThickness = 10f;
    
    public Vector2 panLimit;

    public float scrollspeed = 20f;
    public float minZoomBoundary = 10f;
    public float maxZoomBoundary = 50f;
	
	// Update is called once per frame
	void Update () {

        Vector3 pos = transform.position;

        pos = KeyControls(pos);

        pos =Zoom(pos);

        pos = ClampScrollingEdges(pos);
      
        transform.position = pos;
	}

    private Vector3 KeyControls(Vector3 pos)
    {
        if (Input.GetKey("w") )
        {
            pos.z += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("s"))
        {
            pos.z -= panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("d") )
        {
            pos.x += panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("a") )
        {
            pos.x -= panSpeed * Time.deltaTime;
        }

        //if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - panBorderThickness)
        //{
        //    pos.z += panSpeed * Time.deltaTime;
        //}

        //if (Input.GetKey("s") || Input.mousePosition.y <= panBorderThickness)
        //{
        //    pos.z -= panSpeed * Time.deltaTime;
        //}

        //if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBorderThickness)
        //{
        //    pos.x += panSpeed * Time.deltaTime;
        //}
        //if (Input.GetKey("a") || Input.mousePosition.x <= panBorderThickness)
        //{
        //    pos.x -= panSpeed * Time.deltaTime;
        //}
        return pos;
    }

    private Vector3 ClampScrollingEdges(Vector3 pos)
    {
        var width = CoreManager.instance.HexMapData.GetWidth();
        var height = CoreManager.instance.HexMapData.GetHeight();
        pos.x = Mathf.Clamp(pos.x, 0, width * 1.777f);
        pos.z = Mathf.Clamp(pos.z, 0, height * 1.548f);
        return pos;
    }

    public Vector3 Zoom(Vector3 pos)
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0f)
        {
         

            float scroll = Input.GetAxis("Mouse ScrollWheel");
            pos.y -= scroll * scrollspeed * 100f * Time.deltaTime;

            //zoom boundary
            pos.y = Mathf.Clamp(pos.y, minZoomBoundary, maxZoomBoundary);
        }
        return pos;
       
    }
}
