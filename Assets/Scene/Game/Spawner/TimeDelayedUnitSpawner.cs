﻿using Assets.Scene.Game.TileMap;
using UnityEngine;


namespace Assets.Scene.Game.Spawner

{
    public class TimeDelayedUnitSpawner : IUnitSpawner
    {

        private float _timeDelay;
        private float _cooldown = 0;
        private CoreManager _coreManager;

        public TimeDelayedUnitSpawner(float timeDelay)
        {
            _timeDelay = timeDelay;
            _cooldown = _timeDelay;
            _coreManager = CoreManager.instance;
        }


        public void Spawn(GameObject parent, int playerOwner, float gameTime)
        {
            if (parent == null)
            {
                Debug.LogError("TimeDelayedUnitSpawner::Spawn() => parent is null");
            }

            if (playerOwner < 0 || playerOwner > _coreManager.PlayerManager.PlayerCount())
            {
                Debug.LogError("TimeDelayedUnitSpawner::Spawn() => cannot create unit for player owner " + playerOwner + ". player does not exist.");
            }

            _cooldown -= gameTime;

            if (_cooldown >= 0)
            {
                return;
            }

            _cooldown = _timeDelay;

            var spawnAtCity = _coreManager.CityManager.GetRandomCity();
            var goToCity = _coreManager.CityManager.GetRandomCity();

            var spawnAtCityTile = HexMapData.GetDataTile(spawnAtCity.X, spawnAtCity.Y);
            var destinationTile = HexMapData.GetDataTile(goToCity.X, goToCity.Y);

            var go = _coreManager.UnitManager.CreateUnit(spawnAtCityTile, parent.transform, playerOwner );
            var data= _coreManager.UnitManager.GetUnitGameObject(go);

            data.Move_To(destinationTile);

        }
    }
}






