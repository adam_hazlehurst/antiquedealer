﻿using UnityEngine;

namespace Assets.Scene.Game.Spawner
{
    public interface IUnitSpawner
    {
        void Spawn(GameObject parent, int playerOwner, float gameTime);
    }
}





