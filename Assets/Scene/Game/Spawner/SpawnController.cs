﻿using Assets.Scene.Game.Spawner;
using UnityEngine;

namespace Assets.Scene.Game.Spawner
{
    public class SpawnController : MonoBehaviour
    {

        IUnitSpawner _spawner;

        private void Start()
        {
            _spawner = new UnitSpawner();
        }


        public void Spawn()
        {
            _spawner.Spawn(CoreManager.instance.gameObject, 1, Time.deltaTime);
        }
    }

}