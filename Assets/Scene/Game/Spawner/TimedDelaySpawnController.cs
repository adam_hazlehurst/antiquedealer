﻿using UnityEngine;

namespace Assets.Scene.Game.Spawner
{
    public class TimedDelaySpawnController : MonoBehaviour
    {
        IUnitSpawner _spawner;
        public float Delay;
        public int PlayerOwner;

        public int MaxCount;
        private int _currentCount;
        // Use this for initialization
        void Start()
        {
            _spawner = new TimeDelayedUnitSpawner(Delay);
        }

        // Update is called once per frame
        void Update()
        {
            var numberOfItemsOwnedBy = CoreManager.instance.UnitManager.GetCountByOwner(PlayerOwner);

            if (numberOfItemsOwnedBy < MaxCount)
            {
                _spawner.Spawn(CoreManager.instance.gameObject, PlayerOwner, Time.deltaTime);
            }
        }
    }

}