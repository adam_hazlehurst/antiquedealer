﻿using Assets.Scene.Game.TileMap;
using UnityEngine;


namespace Assets.Scene.Game.Spawner

{
    public class UnitSpawner : IUnitSpawner
    {
        private CoreManager _coreManager;

        public UnitSpawner()
        {
            _coreManager = CoreManager.instance;
        }

        public void Spawn(GameObject parent, int playerOwner, float gameTime)
        {
            if (parent == null)
            {
                Debug.LogError("TimeDelayedUnitSpawner::Spawn() => parent is null");
            }

            if (playerOwner < 0 || playerOwner > _coreManager.PlayerManager.PlayerCount())
            {
                Debug.LogError("TimeDelayedUnitSpawner::Spawn() => cannot create unit for player owner " + playerOwner + ". player does not exist.");
            }

            var city = _coreManager.CityManager.GetRandomCity();

            _coreManager.UnitManager.CreateUnit( HexMapData.GetDataTile(city.X, city.Y), parent.transform, playerOwner);

        }
    }
}





