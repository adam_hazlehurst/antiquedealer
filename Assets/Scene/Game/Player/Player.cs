﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scene.Game.Player
{
    public class Player
    {
        private int _id;
        public string Name;
        public decimal Cash;
        public ColorEnum Color;


        public Player(int id, string name, decimal cash)
        {
            _id = id;
            Name = name;
            Cash = cash;

        }

        public void SetColor(ColorEnum color)
        {
            Color = color;
        }

        public ColorEnum GetColor()
        {
            return Color;
        }

        public int GetId()
        {
            return _id;
        }
    }
}

public enum ColorEnum
{
    WHITE,
    RED,
    BLUE,
    GREEN,
    YELLOW,
    PINK,
    BROWN,
    ORANGE,
    TURQUIOSE
}

