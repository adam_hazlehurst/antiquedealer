﻿
/// <summary>
/// Will store player config into persistant media and will load it.
/// </summary>
/// 
namespace Assets.Scene.Game.Player
{
    public class PlayerConfiguration
    {
        public int NumberOfPlayers;
        public int Difficulty;

        public PlayerConfiguration(int playerCount)
        {
            NumberOfPlayers = playerCount;
        }

        public void Save()
        {

        }

        public void Load()
        {

        }
    }
}
