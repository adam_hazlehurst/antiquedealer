﻿namespace Assets.Scene.Game.Player
{
    public class PlayerLoader
    {
        private PlayerConfiguration _playerConfig;
        private PlayerManager _playerManager;


        public PlayerLoader(PlayerConfiguration playerConfig, PlayerManager playerManager)
        {
            _playerConfig = playerConfig;
            _playerManager = playerManager;
        }

        public void Load()
        {

            for (int i = 0; i < _playerConfig.NumberOfPlayers; i++)
            {
                _playerManager.Create();
            }
        }


    }
}