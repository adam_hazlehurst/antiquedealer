﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scene.Game.Models;
using UnityEngine;


namespace Assets.Scene.Game.Player
{
    public class PlayerManager
    {
        private static int _count = 0;
        List<Player> _players;

        public PlayerManager()
        {
            _players = new List<Player>();
        }

        public Player Get(int id)
        {
            return _players.SingleOrDefault(x => x.GetId() == id);

        }

        public int PlayerCount()
        {
            return _players.Count();
        }


        public Player Create()
        {
            _count++;

            var player = new Player(_count, "Player " + _count, 20m);

            player.SetColor(GetAvailableColor());

            _players.Add(player);

            return player;
        }

        private ColorEnum GetAvailableColor()
        {
            var availableColors = Enum.GetValues(typeof(ColorEnum)).Cast<ColorEnum>().ToList();
            var white = availableColors.Single(x => x == ColorEnum.WHITE);

            availableColors = availableColors.Except<ColorEnum>(GetTakenColor()).ToList();

            availableColors.Remove(white);
            int rnd = UnityEngine.Random.Range(0, availableColors.Count());

            return availableColors[rnd];
        }
        public List<ColorEnum> GetTakenColor()
        {
            var takenColors = new List<ColorEnum>();

            takenColors = _players.Select(x => x.Color).ToList();

            return takenColors;
        }

    }
}