﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileMaterialLoader
{

    private Dictionary<string, Material> _tileMaterials;
   
    public TileMaterialLoader()
    {
        _tileMaterials = new Dictionary<string, Material>();
        Load();
    }

    private void Load()
    {
        var tundra =Resources.Load<Material>("Tile/Material/Tundra");
        _tileMaterials.Add("FLAT_TUNDRA", tundra);

        var grass = Resources.Load<Material>( "Tile/Material/Grass");
        _tileMaterials.Add("FLAT_GRASS", grass);

        var water = Resources.Load<Material>( "Tile/Material/Water");
        _tileMaterials.Add("FLAT_WATER", water);

        var plain = Resources.Load<Material>( "Tile/Material/Plain");
        _tileMaterials.Add("FLAT_PLAIN", plain);

        var desert = Resources.Load<Material>("Tile/Material/Desert");
        _tileMaterials.Add("FLAT_DESERT", desert);

        var hillTundra = Resources.Load<Material>("Tile/Hills/Materials/Tundra");
        _tileMaterials.Add("HILL_TUNDRA", hillTundra);

        var hillgrass = Resources.Load<Material>( "Tile/Hills/Materials/Hill_Grass");
        _tileMaterials.Add("HILL_GRASS", hillgrass);

        var hillplain = Resources.Load<Material>("Tile/Hills/Materials/Plain");
        _tileMaterials.Add("HILL_PLAIN", hillplain);

        var hilldesert = Resources.Load<Material>("Tile/Hills/Materials/Desert");
        _tileMaterials.Add("HILL_DESERT", hilldesert);

        var mountain = Resources.Load<Material>("Tile/Mountain/Materials/Mountain_Grass");
        _tileMaterials.Add("MOUNTAIN_GRASS", mountain);
    }

    public int GetMaterialCount()
    {
        return _tileMaterials.Count;
    }

    public Material Get(string name)
    {
        return _tileMaterials[name];
    }

    public Material GetRandom()
    {
        int rnd = Random.Range(0,_tileMaterials.Count);
        return _tileMaterials.Values.ToArray()[rnd];
    }


}
