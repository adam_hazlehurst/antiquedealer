﻿using Assets.Scene.Game.TileMap;

public static class MapHelper
{

    public static readonly float WaterFreshold = -.0f;
    public static readonly float HillFreshhold = .4f;
    public static readonly float MountainFreshhold = .55f;

    public static readonly float IceFreshhold = -.3f; //all below this is ice
    public static readonly float GrassFreshhold = 0.4f;// all below this is grass
    public static readonly float PlainFreshhold = .55f; // all below this is plain

   
    public static bool IsHot(TileData data_tile)
    {
        return data_tile.temperature >= PlainFreshhold;
    }

    public static bool IsWarm(TileData data_tile)
    {
        return data_tile.temperature >= GrassFreshhold && data_tile.temperature < PlainFreshhold;
    }

    public static bool IsCool(TileData data_tile)
    {
        return data_tile.temperature >= IceFreshhold && data_tile.temperature < GrassFreshhold;
    }

    public static bool IsFreezing(TileData data_tile)
    {
        return data_tile.temperature < IceFreshhold;
    }

    public static bool IsLowlands(TileData data_tile)
    {
        return data_tile.elevation >= 0f && data_tile.elevation < HillFreshhold;
    }

    public static bool IsWater(TileData data_tile)
    {
        return data_tile.elevation < WaterFreshold;
    }

    public static bool IsMountain(TileData data_tile)
    {
        return data_tile.elevation >= MountainFreshhold;
    }

    public static bool IsHill(TileData data_tile)
    {
        return data_tile.elevation >= HillFreshhold && data_tile.elevation < MountainFreshhold;
    }
}
