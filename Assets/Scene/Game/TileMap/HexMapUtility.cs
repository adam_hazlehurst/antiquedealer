﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scene.Game.TileMap
{
    public static class HexMapUtility
    {
        public const float XOffSet = 1.73f;
        public const float ZOffset = 1.50f;

        public static float GetWorldYCoords(int y)
        {
            return y * ZOffset;
        }

        public static float GetWorldXCoords(int x, int y)
        {
            float xPos = x * XOffSet;

            if (y % 2 == 1)
            {
                xPos += XOffSet / 2f;
            }
            return xPos;
        }
    }
}
