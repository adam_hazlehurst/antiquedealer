﻿

using Assets.Scene.Game.Models;
using QPath;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scene.Game.TileMap
{
    public class TileData: IQPathTile
    {
        public int X;
        public int Y;
        private float _baseCostToEnter = 1f;

        public float elevation;
        public float temperature;
        public float moisture;
        TileData[] neighbours;
        private bool _isCityResearved = false;
        private List<UnitData> _units;
        private City _city;
        
        public TileTypeEnum Type;

        private Action<TileData> cbOnTypeChanged;
        private Action<List<UnitData>, UnitData> cbOnVehicleArrived;
        private Action<List<UnitData>, UnitData> cbOnVehicleDeparted;

        public void ChangeType(TileTypeEnum v)
        {
            if (Type != v)
            {
                Type = v;

                if (cbOnTypeChanged != null)
                {
                    cbOnTypeChanged(this);
                }
            }
        }
                      
        public static float Distance(IQPathTile a, IQPathTile b)
        {
            TileData atile = (TileData)a;
            TileData bTile = (TileData)b;
            return Vector3.Distance(new Vector3(atile.X, 0, atile.Y), new Vector3(bTile.X, 0, bTile.Y));

        }

        public  string GetTemperatureDisplay()
        {

            if (MapHelper.IsFreezing(this))
                return "Sub Zero";
            if (MapHelper.IsCool(this))
                return "Cold";
            if (MapHelper.IsWarm(this))
                return "Warm";
            if (MapHelper.IsHot(this))
                return "Hot";

            return "error";
        }

        public bool IsResearved()
        {
            return _isCityResearved;
        }

        public void SetNeighboursAsCityResearved()
        {
            var neigbours  =GetNeighbours();
            foreach(var neighbour in neigbours)
            {
                var n = (TileData)neighbour;
                n._isCityResearved = true;
            }
        }

        public void RegisterOnTypeChanged(Action<TileData> onChanged)
        {
            cbOnTypeChanged += onChanged;
        }

        
        public void RegisterOnVehicleArrived(Action<List<UnitData>, UnitData> onVehicleArrived)
        {
            cbOnVehicleArrived = onVehicleArrived;
        }
        public void RegisterOnVehicleDeparted(Action<List<UnitData>, UnitData> onVehicleDeparted)
        {
            cbOnVehicleDeparted = onVehicleDeparted;
        }

        public TileData()
        {
            _units = new List<UnitData>();
            
        }

        public void SetCity(City city)
        {
            _city = city;
        }
        public City GetCity()
        {
            return _city;
        }

        public IQPathTile[] GetNeighbours()
        {
            if (this.neighbours != null)
            {
                return this.neighbours;
            }

            List<TileData> neighbours = new List<TileData>();
            if (Y % 2 == 0)
            {
                neighbours.Add(HexMapData.GetDataTile(X, Y + 1)); // ne
                neighbours.Add(HexMapData.GetDataTile(X + 1, Y)); //east
                neighbours.Add(HexMapData.GetDataTile(X, Y - 1)); // se
                neighbours.Add(HexMapData.GetDataTile(X - 1, Y - 1)); //sw
                neighbours.Add(HexMapData.GetDataTile(X - 1, Y ));  //west  
                neighbours.Add(HexMapData.GetDataTile(X - 1, Y + 1));
            }
            else
            {
                neighbours.Add(HexMapData.GetDataTile(X + 1, Y+1));
                neighbours.Add(HexMapData.GetDataTile(X + 1, Y));
                neighbours.Add(HexMapData.GetDataTile(X + 1, Y - 1));
                neighbours.Add(HexMapData.GetDataTile(X, Y - 1));
                neighbours.Add(HexMapData.GetDataTile(X -1, Y ));
                neighbours.Add(HexMapData.GetDataTile(X , Y + 1));
            }

            List<TileData> neighbours2 = new List<TileData>();

            foreach(var tile in neighbours)
            {
                if(tile != null)
                {
                    neighbours2.Add(tile);
                }
            }

            this.neighbours = neighbours2.ToArray();

            return this.neighbours;
        }

        public List<UnitData> GetUnits()
        {
            return _units;
        }

        public float AggregateCostToEnter(float costSoFar, IQPathTile sourceTile, IQPathUnit unit)
        {
            if(elevation <0)
            {
                _baseCostToEnter += 999f;
            }
            if(MapHelper.IsHill(this))
            {
                _baseCostToEnter +=.27f;
            }
            if (MapHelper.IsMountain(this))
            {
                _baseCostToEnter += .8f;
            }
            return _baseCostToEnter; 
        }

        public void RemoveUnit(UnitData unit)
        {
            if (_units.Contains(unit))
            {
                if (cbOnVehicleDeparted != null)
                {
                    cbOnVehicleDeparted(_units, unit);
                }
                _units.Remove(unit);
            }
        }

        public void AddUnit(UnitData unit)
        {
            if (!_units.Contains(unit))
            {
                if (cbOnVehicleArrived != null)
                {
                    Debug.Log("arrived.");
                    cbOnVehicleArrived(_units, unit);
                }
                _units.Add(unit);
            }
        }

        public string GetElevationDisplay()
        {
            if (MapHelper.IsWater(this))
                return "Water";
            if (MapHelper.IsLowlands(this))
                return "Lowlands";
            if (MapHelper.IsHill(this))
                return "Hills";
            if (MapHelper.IsMountain(this))
                return "Mountain";

            return "error";
        }
    }
}
