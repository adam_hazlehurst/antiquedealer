﻿using QPath;
using System;
using UnityEngine;

namespace Assets.Scene.Game.TileMap
{
    public  class HexMapData: IQPathWorld
    {
        private static TileData[,] _tiles;
        private static int _width;
        private static int _height;
        private TileMaterialLoader _tileMaterialLoader;
        private TileElevation _tileElevation;
        private TileMoisture _tileMoisture;
        private TileTemperature _tileTemperature;

        public HexMapData(int width, int height, TileMaterialLoader tileMaterialLoader )
        {
            _width = width;
            _height = height;
            _tileMaterialLoader = tileMaterialLoader;
            _tileElevation = new TileElevation();
            _tileMoisture = new TileMoisture();
            _tileTemperature = new TileTemperature();

            GenerateMap(width, height);
        }

        private void GenerateMap(int width, int height)
        {
            _tiles = new TileData[width, height];
            float lowTemp = 0f;
            float highTemp = 0f;
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    TileData tile = new TileData();
                    tile.X = x;
                    tile.Y = y;
                    tile.elevation = _tileElevation.SetElevation(x, y, height, width, tile);
                    tile.temperature = _tileTemperature.SetTemperature(x, y, height, width, tile);
                    if(tile.temperature>highTemp) { highTemp = tile.temperature; }
                    if (tile.temperature < lowTemp) { lowTemp= tile.temperature; }
                    tile.moisture = _tileMoisture.SetMoisture(x, y, height, width, tile);
                    _tiles[x, y] =tile;
                }
            }

        }

        private float SetMoisture(int x, int y, int height, int width, TileData tile)
        {
            return 0f;
        }

        private float SetTemperature(int x, int y, int height, int width, TileData tile)
        {
            return _tileTemperature.SetTemperature(x, y, height, width, tile);
        }

        private TileTypeEnum CalculateNextType()
        {
            var matCount=  _tileMaterialLoader.GetMaterialCount();
            int rnd = UnityEngine.Random.Range(0, matCount);

            return (TileTypeEnum)rnd;
        }

        public int GetWidth()
        {
            return _width;
        }
        public int GetHeight()
        {
            return _height;
        }

        public static TileData GetDataTile(int x, int y)
        {
            if((x < 0 || x >= _width) || (y < 0 || y >= _height))
            {
                return null;
            }
            
            return _tiles[x, y];
        }
     
    }
}
