﻿using UnityEngine;

namespace Assets.Scene.Game.TileMap
{
    public class TileTemperature
    {
        public float SetTemperature(int x, int y, int height, int width, TileData tile)
        {
            //dist==1:: 1/10 = 0.1 :: .5f - 0.1f = .4f (hot)
            ///dist==10::10/10 = 1 :: .5f - 1 = -.5f (cold);
            float noiseModifier = 7f;

            var equator = height / 2;

            var distanceFromEquator = y - equator;

            if (distanceFromEquator < 0) { distanceFromEquator *= -1; }

            float maxModifer = .5f;

            float mod = (float)distanceFromEquator / equator;

            var result = maxModifer - mod + .05f;

            if(tile.elevation > MapHelper.HillFreshhold && tile.elevation <MapHelper.MountainFreshhold) { result -= UnityMathHelper.Range(0f,0.3f); }
            if (tile.elevation > MapHelper.MountainFreshhold) { result -= UnityMathHelper.RandomFloat01(); }

            var  perl = Mathf.PerlinNoise(x + UnityMathHelper.RandomFloat01(), y + UnityMathHelper.RandomFloat01());
            result = result + (perl / noiseModifier);


            return result;
        }

        

        private float DistanceFromEquator(int y, float equator)
        {
            float result = 0;

            var distanceFromEquator = y - equator;
            if(distanceFromEquator<0) { distanceFromEquator *= -1; }


            return result;
        }
    }
}
