﻿using System.Collections.Generic;
using UnityEngine;

public class TileLoader
{
    private Dictionary<string, GameObject> _tileSourcePrefabs;
    
    private string _baseTilePath = "Tile";
    
    public TileLoader()
    {
        _tileSourcePrefabs = new Dictionary<string, GameObject>();
        Load();
    }

    public void Load()
    {
        _tileSourcePrefabs.Add("flat", Resources.Load<GameObject>(_baseTilePath + "/Hex"));
        _tileSourcePrefabs.Add("hill", Resources.Load<GameObject>(_baseTilePath + "/Hills/Hill"));
        _tileSourcePrefabs.Add("mountain", Resources.Load<GameObject>(_baseTilePath + "/Mountain/Mountain"));
    }

    public GameObject CreateTile(string name)
    {
        return _tileSourcePrefabs[name];
    }
}
