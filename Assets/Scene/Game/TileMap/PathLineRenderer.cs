﻿using Assets.Scene.Game.Models;
using Assets.Scene.Game.TileMap;
using QPath;
using UnityEngine;

public class PathLineRenderer
{

    private LineRenderer _lineRenderer;
    private UnitData _selectedUnit;

    private void RegiserOnDestinationReached(UnitData unit)
    {
        _lineRenderer.enabled = false;
        _selectedUnit.UnRegiesterOnDestinationReached(RegiserOnDestinationReached);
    }


    public void Draw(LineRenderer lineRenderer,  IQPathTile[] tilePath, UnitData selectedUnit)
    {
        _lineRenderer = lineRenderer;
        _selectedUnit = selectedUnit;
        _selectedUnit.RegisterOnDestinationReached(RegiserOnDestinationReached);

        if (tilePath.Length == 0)
        {
            _lineRenderer.enabled = false;
            return;
        }

        _lineRenderer.enabled = true;

        Vector3[] ps = new Vector3[tilePath.Length];

        for (int i = 0; i < tilePath.Length; i++)
        {
            GameObject go = CoreManager.instance.GetMap().GetTile((TileData)tilePath[i]);
            ps[i] = go.transform.position + new Vector3(0, .2f, 0);

        }
        _lineRenderer.positionCount = ps.Length;
        _lineRenderer.SetPositions(ps);
        
    }
}

