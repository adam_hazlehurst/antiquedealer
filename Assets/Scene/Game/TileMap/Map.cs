﻿using Assets.Scene.Game.Cities;
using Assets.Scene.Game.TileMap;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour {

    //public Dictionary<TileData, GameObject> TileGameObjects;

    private HexMapData _hexMapData;
    private CityManager _cityManager;
    private GameObject _parent;
    private TileLoader _tileLoader;
    private TileMaterialLoader _tileMaterialLoader;
    
    private Dictionary<GameObject, TileData> _gameObjectsToTileDataDictionary;
    private Dictionary<TileData, GameObject> _tileDataFromGameObject;


    public  Map(GameObject go_parent,HexMapData hexMap, CityManager cityManager,  TileMaterialLoader materialLoader)
    {
        _tileMaterialLoader = materialLoader;
        _hexMapData = hexMap;
        _parent = go_parent;
        _cityManager = cityManager;
        _gameObjectsToTileDataDictionary = new Dictionary<GameObject, TileData>();
        _tileLoader = new TileLoader();
        _tileDataFromGameObject = new Dictionary<TileData, GameObject>();
    }
   
    public void RenderMap()
    {
        for (int x = 0; x < _hexMapData.GetWidth(); x++)
        {
            for (int y = 0; y < _hexMapData.GetHeight(); y++)
            {
                var data_tile = HexMapData.GetDataTile(x, y);

                var hexPrefab = CreateTileType(data_tile);

                GameObject hex_go = (GameObject)Instantiate(hexPrefab, new Vector3(HexMapUtility.GetWorldXCoords(x,y), 0, HexMapUtility.GetWorldYCoords(y)), Quaternion.identity);

                SetupTileAsGameObject(hex_go, x,y);

                hex_go = SetupTileMaterial(data_tile, hex_go);

                LinkGameObjectAndTileData(hex_go, data_tile);

                RenderStarterCities(x,y,hex_go);

                hex_go.transform.SetParent(_parent.transform);
            }
        }
    }

    private GameObject SetupTileMaterial(TileData data_tile, GameObject hex_go)
    {
        MeshRenderer mr = hex_go.GetComponentInChildren<MeshRenderer>();
        if (MapHelper.IsWater(data_tile))
        {
            mr.material = _tileMaterialLoader.Get("FLAT_WATER");
            hex_go.transform.position -= new Vector3(0, .2f, 0);
        }
        else if(MapHelper.IsLowlands(data_tile))
        {
            mr.material = SetMaterialToLowLands(data_tile);
        }
        else if(MapHelper.IsHill(data_tile))
        {
            mr.material   =SetMaterialToHill(data_tile);
        }
        else if (MapHelper.IsMountain(data_tile))
        {
            mr.material = _tileMaterialLoader.Get("MOUNTAIN_GRASS");
        }

        return hex_go;
    }

    private Material SetMaterialToLowLands(TileData data_tile)
    {
        if (MapHelper.IsFreezing(data_tile))
        {
           return _tileMaterialLoader.Get("FLAT_TUNDRA");
        }
        if (MapHelper.IsCool(data_tile))
        {
            return _tileMaterialLoader.Get("FLAT_GRASS");
        }
        else if (MapHelper.IsWarm(data_tile))
        {
            return _tileMaterialLoader.Get("FLAT_PLAIN");
        }
        else if (MapHelper.IsHot(data_tile))
        {
            return _tileMaterialLoader.Get("FLAT_DESERT");
        }

        return _tileMaterialLoader.Get("FLAT_GRASS");
    }

    private Material SetMaterialToHill(TileData data_tile)
    {
        if (MapHelper.IsFreezing(data_tile))
        {
           return _tileMaterialLoader.Get("HILL_TUNDRA");
        }
        else if (MapHelper.IsCool(data_tile))
        {
            return _tileMaterialLoader.Get("HILL_GRASS");
        }
        else if (MapHelper.IsWarm(data_tile))
        {
            return _tileMaterialLoader.Get("HILL_PLAIN");
        }
        else if (MapHelper.IsHot(data_tile))
        {
            return _tileMaterialLoader.Get("HILL_DESERT");
        }

        return _tileMaterialLoader.Get("HILL_GRASS");
    }
   

    private GameObject MapMoisture(TileData data_tile, GameObject hexPrefab)
    {
        return hexPrefab;
    }

    private GameObject CreateTileType(TileData data_tile)
    {
        GameObject hexPrefab = _tileLoader.CreateTile("flat");

        if (data_tile.elevation > MapHelper.HillFreshhold && data_tile.elevation <= MapHelper.MountainFreshhold)
        {
            hexPrefab = _tileLoader.CreateTile("hill");
        }
        else if (data_tile.elevation > MapHelper.MountainFreshhold)
        {
            hexPrefab = _tileLoader.CreateTile("mountain");
        }

        return hexPrefab;
    }

    public TileData GetTile(GameObject obj)
    {
        if (!_gameObjectsToTileDataDictionary.ContainsKey(obj))
        {
            Debug.LogError("Map::GetTileFromGameObject() Could not find Key");
            return null;
        }
        return _gameObjectsToTileDataDictionary[obj];


    }
    public GameObject GetTile(TileData obj)
    {
        if (!_tileDataFromGameObject.ContainsKey(obj))
        {
            Debug.LogError("Map::GetTileFromGameObject() Could not find Key");
            return null;
        }
        return _tileDataFromGameObject[obj];


    }


    private void SetupTileAsGameObject(GameObject hex_go, int x, int y)
    {
        var wrapper = hex_go.GetComponentInChildren<HexTileDataWrapper>();
        wrapper.x = x;
        wrapper.y = y;

        hex_go.name = "Hex_" + x + "_" + y;

        hex_go.isStatic = true;

    }

    private void LinkGameObjectAndTileData(GameObject hex_go, TileData data_tile)
    {
        _tileDataFromGameObject.Add(data_tile, hex_go);
        _gameObjectsToTileDataDictionary.Add(hex_go, data_tile);
    }
  
    private void RenderStarterCities(int x, int y, GameObject hex_go)
    {
        foreach (var city in _cityManager.GetCities())
        {

            if (city.Key.X == x && city.Key.Y == y)
            {
                MeshRenderer mr = hex_go.GetComponentInChildren<MeshRenderer>();
                mr.material.color = Color.grey;
            }
        }
    }



}
