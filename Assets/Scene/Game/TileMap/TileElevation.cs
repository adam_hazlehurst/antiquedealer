﻿using UnityEngine;

namespace Assets.Scene.Game.TileMap
{
    public class TileElevation
    {
        private bool _waterEdges = true;
        private float _noiseResolution = .15f;
        private float _noiseScale = 1f;
        private Vector2 noiseOffset;

        public float SetElevation(int x, int y, int height, int width, TileData tile)
        {

            if (IsTileAtMapEdge(x, y, width, height))
            {
                return -5f;
            }

            noiseOffset = new Vector2(UnityMathHelper.RandomFloat01(), UnityMathHelper.RandomFloat01());

            var xshift = ((float)x / width / _noiseResolution);
            var yshift =((float)y / height / _noiseResolution);

            var p = Mathf.PerlinNoise(
                xshift + noiseOffset.x,
                yshift + noiseOffset.y);

            p +=_noiseScale;
            p -= 1.22f; // bigger negative = more land

            return p;

        }
        

        private bool IsTileAtMapEdge(int x, int y, int width, int height)
        {
            if (_waterEdges)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
