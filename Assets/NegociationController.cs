﻿using Assets.Scene.Game.Negociation;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NegociationController : MonoBehaviour {

    private Negociation _negociations;

    public GameObject TeamSellerPanel;
    private List<GameObject> _sellerItems;

    public GameObject TeamBuyerPanel;
    private List<GameObject> _buyerItems;

    public GameObject ItemObjectPanel;
    private List<GameObject> _basketItemObjects;


    public GameObject TeamMemberLineRow;
    public GameObject BasketItemLineRow;

    private bool _hasChanged = false;

    // Use this for initialization
    void Start () {
        _negociations = new Negociation(new BasicList<GameThing>(), new BasicList<GameThing>(), new BasicList<GameThing>());

        _sellerItems = new List<GameObject>();
        _buyerItems = new List<GameObject>();
        _basketItemObjects = new List<GameObject>();

        _negociations.TeamBuyers.Register_OnAdded(OnPersonAddedToTeam);
        _negociations.TeamSellers.Register_OnAdded(OnPersonAddedToTeam);
        _negociations.Basket.Register_OnAdded(OnItemObjectAdded);

        _hasChanged = true;

	}

    public void OnPersonAddedToTeam(GameThing buyer)
    {
        _hasChanged = true;
    }
    public void OnItemObjectAdded(GameThing item)
    {
        _hasChanged = true;
    }

    public void AddToBuyer()
    {
        var p = new Person();
        p.SetName("another buyer");
        _negociations.TeamBuyers.Add(p);
    }
    public void AddToSeller()
    {
        var p = new Person();
        p.SetName("another seller");
        _negociations.TeamSellers.Add(p);
    }
    public void AddBasketItem()
    {
        var p = new Item();
        p.SetName("drum");
        _negociations.Basket.Add(p);
    }
	
	// Update is called once per frame
	void Update () {
        if (_hasChanged)
        {
            Debug.Log("T");

            ClearSellerAndBuyerVisuals();

            Draw(_negociations.TeamBuyers, TeamMemberLineRow, TeamBuyerPanel.transform, _buyerItems);
            Draw(_negociations.TeamSellers, TeamMemberLineRow, TeamSellerPanel.transform, _sellerItems);
            Draw(_negociations.Basket, BasketItemLineRow, ItemObjectPanel.transform, _basketItemObjects);
            
            _hasChanged = false;
        }
	}



    public void Draw<T>(BasicList<T> list, GameObject itemToRender, Transform parent,List<GameObject> goDictionary) where T : IGameThing
    {
        for (int x = 0; x < list.Count(); x++)
        {
            var go = Instantiate(itemToRender);
            go.gameObject.GetComponentInChildren<Text>().text = list.GetItemAt(x).GetName();
            go.transform.SetParent(parent);
            goDictionary.Add(go);
        }
    }

    private void ClearSellerAndBuyerVisuals()
    {
        for (int x = 0; x < _sellerItems.Count; x++)
        {
            Destroy(_sellerItems[x]);
        }

        for (int x = 0; x < _buyerItems.Count; x++)
        {
            Destroy(_buyerItems[x]);
        }
        for (int x = 0; x < _basketItemObjects.Count; x++)
        {
            Destroy(_basketItemObjects[x]);
        }
    }
}


