﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiButtonPanelController : MonoBehaviour {

    public GameObject Modal;
    private List<GameObject> _goList;

    private Dictionary<string, GameObject> _modalPanels;

    void Start()
    {
        _modalPanels = new Dictionary<string, GameObject>();
        _modalPanels.Add("base", Resources.Load<GameObject>("UI/Modal/ModalContainer"));
        _modalPanels.Add("negociate", Resources.Load<GameObject>("UI/Modal/NegociationModalContent"));
    }

    public void Open(string formToOpen)
    {
        Modal.SetActive(!Modal.activeInHierarchy);

        var modalBody = Modal.transform.Find("ModalBody");

        var ModalContainerContent = Instantiate(_modalPanels["base"]).transform.Find("ModalContentPage");

        var negociate_go = Instantiate(_modalPanels["negociate"]);

        negociate_go.transform.SetParent(ModalContainerContent.transform, false);
        ModalContainerContent.SetParent(modalBody.transform, false);


    }

}
