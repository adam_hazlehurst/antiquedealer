﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavesRenderer : MonoBehaviour {


    public int Dimension = 10;
    protected MeshFilter meshFilter;
    protected Mesh mesh;
    public float UVScale;
    public Octave[] octaves;


	void Start () {
        mesh = new Mesh();
        mesh.name = gameObject.name;
        mesh.vertices = GenerateVerts();
        mesh.triangles = GenerateTriangles();
        mesh.uv = GenerateUVs();
        mesh.RecalculateBounds();

        meshFilter = gameObject.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;
	}

    private Vector2[] GenerateUVs()
    {
        var uvs = new Vector2[mesh.vertices.Length];

        for (int x = 0; x <= Dimension; x++)
        {
            for (int z = 0; z <= Dimension; z++)
            {
                var vec = new Vector2((x / UVScale) % 2, (z / UVScale) % 2);
                uvs[Index(x, z)] = new Vector2(vec.x <= 1 ? vec.x : 2 - vec.x, vec.y <= 1 ? vec.y : 2 - vec.y);
            }
        }
        return uvs;

    }

    private Vector3[] GenerateVerts()
    {
        var verts = new Vector3[(Dimension + 1) * (Dimension + 1)];

        for (int x = 0; x < Dimension; x++)
        {
            for (int z = 0; z < Dimension; z++)
            {
                verts[Index(x, z)] = new Vector3(x, 0, z);
            }
        }
        return verts;
    }

    private int Index(int x, int z)
    {
        return x * (Dimension + 1) + z;
    }

    private int[] GenerateTriangles()
    {
        var tries = new int[mesh.vertices.Length * 6];
        for (int x = 0; x < Dimension; x++)
        {
            for (int z = 0; z < Dimension; z++)
            {
                tries[Index(x, z) * 6 + 0] = Index(x, z);
                tries[Index(x, z) * 6 + 1] = Index(x+1, z+1);
                tries[Index(x, z) * 6 + 2] = Index(x+1, z);
                tries[Index(x, z) * 6 + 3] = Index(x, z);
                tries[Index(x, z) * 6 + 4] = Index(x, z+1);
                tries[Index(x, z) * 6 + 5] = Index(x+1, z+1);
            }
        }
        return tries;
    }

    

    void Update () {
        var verts = mesh.vertices;
        for (int x = 0; x <= Dimension; x++)
        {
            for (int z = 0; z <= Dimension; z++)
            {
                var y = 0f;
                for (int o = 0; o < octaves.Length; o++)
                {
                    if (octaves[o].alternate)
                    {
                        var perl = Mathf.PerlinNoise((x * octaves[o].scale.x) / Dimension, (z * octaves[o].scale.y) / Dimension) * Mathf.PI * 2f;
                        y += Mathf.Cos(perl+ octaves[o].speed.magnitude * Time.time) * octaves[o].height;
                    }
                    else if (octaves[o].alternate)
                    {
                        var perl = Mathf.PerlinNoise((x * octaves[o].scale.x + Time.time * octaves[o].speed.x) / Dimension, (z * octaves[o].scale.y + Time.time * octaves[o].speed.y) / Dimension) - 0.5f; ;
                        y += perl * octaves[o].height;
                    }
                }

                verts[Index(x, z)] = new Vector3(x, y, z);
            }


        }
        mesh.vertices = verts;
	}

    [Serializable]
    public struct Octave
    {
        public Vector2 speed;
        public Vector2 scale;
        public float  height;
        public bool alternate;

    }
}
